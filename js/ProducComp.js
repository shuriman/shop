
Vue.component('products', {
    data(){
        return {
            catalogUrl: '',
            products: [],
            filtered: [],
            imgCatalog: 'img',
        }
    },
    methods: {
        filter(value){
            let regexp = new RegExp(value, 'i');
            this.filtered = this.products.filter(el => regexp.test(el.name));
        }
    },
    mounted(){
        this.$parent.getJson(`${API}/api/products`)
            .then(data => {
                for(let el of data){
                    this.products.push(el);
                    this.filtered.push(el);
                }
            });
    },
    template: `
        <div class="items">
            <product v-for="item of filtered" :key="item.id" :img="item.image" :alt="item.description" :product="item"></product>
        </div>
    `
});

Vue.component('product', {
    props: ['product', 'img', 'alt'],
    data() {
      return {
          cartAPI: this.$root.$refs.cart,
      };
    },

    template: `
        <div class="item">
            <a class="item__link" href="#">
                <img class="item__img" :src="img" :alt="alt">
                <div class="item__desc">
                    <p class="item__desc">{{ product.name }}</p>
                    <p class="item__price">$ {{ product.price }}</p>
                </div>
            </a>
            <div class="add-box">
                <a @click="cartAPI.addProduct(product)" class="add">
                    <img class="" src="img/basket.svg" alt="add to basket">
                    <p class="add-txt">Add to Cart</p>
                </a>
            </div>
        </div>`

//         `
//     <div class="product-item">
//                 <img :src="img" alt="Some img">
//                 <div class="desc">
//                     <h3>{{product.product_name}}</h3>
//                     <p>{{product.price}}₽</p>
//                     <button class="buy-btn" @click="cartAPI.addProduct(product)">Купить</button>
// <!-- 1                    <button class="buy-btn" @click="$root.$refs.cart.addProduct(product)">Купить</button>-->
// <!-- 2                    <button class="buy-btn" @click="$parent.$parent.$refs.cart.addProduct(product)">Купить</button>-->
//                 </div>
//             </div>
//     `
});
