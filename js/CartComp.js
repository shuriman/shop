Vue.component('cart', {
    data(){
      return {
          imgCart: 'https://via.placeholder.com/50x100',
          cartUrl: '/getBasket.json',
          cartItems: [],
          showCart: true,
      }
    },
    methods: {
        addProduct(product){
            let find = this.cartItems.find(el => el.id === product.id);
            if(find){
                this.$parent.putJson(`${API}/api/cart/${find.id}`, {quantity: 1})
                    .then(data => {
                        find.quantity++;
                    });
            } else {
                let prod = Object.assign({quantity: 1}, product);
                this.$parent.postJson(`${API}/api/cart`, prod)
                  .then(data => {
                          this.cartItems.push(prod);
                  });
            }
            this.showCart = true;

        },
        remove(item) {
            this.$parent.deleteJson(`${API}/api/cart/${item.id}`)
                .then(data => {
                    if(data !== null && typeof data !== 'undefined') {
                        this.cartItems.splice(this.cartItems.indexOf(item), 1)
                        if (this.cartItems.length === 0)
                            this.showCart = false;
                    }
                });
        }
    },
    mounted(){
        this.$parent.getJson(`${API}/api/cart`)
            .then(data => {
                if(data !== null && typeof data !== 'undefined')
                    for(let el of data.contents){
                        this.cartItems.push(el);
                    }
            });
    },
    computed: {
        totalSum: function () {
            let totalSum = 0;
            for(let el of this.cartItems){
                totalSum += (parseInt(el.price) * parseInt(el.quantity));
            }

            return totalSum;

        }

    },
    template: `
            <div class="basket__menu" v-show="showCart">
                <div class="basket__items" >
                    <cart-item v-for="item of cartItems" 
                        :key="item.id" 
                        :img="item.image" 
                        :alt="item.description" 
                        :rating="item.rating"
                        :cartItem="item">
                        @remove="remove"
                        @getRating="getRating"
                    </cart-item>
                </div>
                
                <div class="basket__total">
                    <div>TOTAL</div> 
                    <div class="basket__totalsum">{{ totalSum }}</div>
                </div>
                                  
                <div class="basket__bottom">
                    <a class="basket__checkout" href="">checkout</a>
                    <a class="basket__gotocart" href="">go to cart</a>
                </div>
            </div>
    `
    // template: `
    //     <div>
    //         <button class="btn-cart" type="button" @click="showCart = !showCart">Корзина</button>
    //         <div class="cart-block" v-show="showCart">
    //             <p v-if="!cartItems.length">Корзина пуста</p>
    //             <cart-item class="cart-item"
    //             v-for="item of cartItems"
    //             :key="item.id_product"
    //             :cart-item="item"
    //             :img="imgCart"
    //             @remove="remove">
    //             </cart-item>
    //         </div>
    //     </div>`
});

Vue.component('cart-item', {
    props: ['cartItem', 'img', 'alt'],
    template: `
        <div class="basket__item">
            <img class="item__image" :src="img" :alt="alt">
            <div class="item__description">
                <h4 class="item__text">{{ cartItem.name }}</h4>
                <i class='fa fa-star basket__star' aria-hidden='true' v-for='v of Array.from(Array(cartItem.rating))' ></i>
                <div class="basket__price">$ {{cartItem.quantity}} X {{ cartItem.price }}</div>
            </div>
            <a class="basket__item-delete" @click="$root.$refs.cart.remove(cartItem)">
                <i class="fas fa-times-circle"></i>
            </a>
        </div>`
});
