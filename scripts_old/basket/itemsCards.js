
const pathToImg = 'img';
const pathToItemImg = `${pathToImg}/featured`;

const basket_items = '.basket__items';
const basket_total = '.basket__totalsum';
const basket__menu = '.basket__menu';
const basketitemscount = '.basketitemscount';

var itemsInCart = [];

function getItemMarkup(item){
    return `<div class="item">
        <a class="item__link" href="#">
            <img class="item__img" src="${pathToItemImg}/${item.image}" alt="${item.description}">
            <div class="item__desc">
                <p class="item__desc">${item.name}</p>
                <p class="item__price">$${item.price}</p>
            </div>
        </a>
        <div class="add-box">
            <a data-item-id="${item.id}" class="add">
                <img class="" src="${pathToImg}/basket.svg" alt="add to basket">
                <p class="add-txt">Add to Cart</p>
            </a>
        </div>
    </div>`;
}

function insertItemsInFeatured(items){
    const featured = '.featured';

    let html = '';
    for(let item of items){
        html += getItemMarkup(item);
    }

    document.querySelector('.featured > .items').innerHTML = html;
}

function addItemRating(rating){
    let ratingHtml = '';
    for(let i = 0; i < rating; i++){
        ratingHtml += `<i class="fa fa-star basket__star" aria-hidden="true"></i>`;
    }

    return ratingHtml;
}

function countExistsItemInCart(itemId){
    let i = 1;
    itemsInCart.forEach(item =>{
        if(item.id === itemId)
            i++;
    });

    return i;
}

function GetItemById(itemId){
    return items.find(i => i.id === itemId);
}

function GetItemInBasketById(itemId){
    return itemsInCart.find(i => i.id === parseInt(itemId));
}

function AddBasketTotal(){
    let total = 0;
    itemsInCart.forEach(p => {
        total += p.price;
    });

    document.querySelector(basket_total).innerText = '$'+total;
}

function RemoveBasketTotal(){
    RemoveItemCount();
    document.querySelector(basket_total).innerHTML = '';
    document.querySelector(basket__menu).style.display = 'none';
}

/*
    item - товар
    itemCount - кол-во товара в корзине
    totalPrice - итоговая цена товаров в корзине

*/
function ParseBasketHtml(item, itemCount){
    return `
        <div class="basket__item" data-item-id="${item.id}">
            <img class="item__image" src="${pathToItemImg}/${item.image}" alt="${item.description}">
            <div class="item__description">
                <h4 class="item__text">${item.name}</h4>
                ${addItemRating(item.rating)}
                <div class="basket__price">${itemCount} x ${item.price}</div>
            </div>
            <div data-item-id="${item.id}" class="basket__item-delete">
                <i class="fas fa-times-circle"></i>
            </div>
        <div>`;
}

function addItemToCart(itemId){
    let basket = document.querySelector(basket_items);
    
    let item = GetItemById(itemId);
    let itemHtml = '';

    if(itemsInCart.length > 0){
        let count = countExistsItemInCart(itemId);
        basket.innerHTML += ParseBasketHtml(item, 1);
        
    }else{
        basket.innerHTML = ParseBasketHtml(item, 1);
    }

    itemsInCart.push(item);
    if(itemsInCart.length > 0)
        document.querySelector(basket__menu).style.display = 'block';

    AddBasketTotal(itemsInCart);
    addEventListenerToRemoveCart();
}

/*
    Показать сколько итемов в корзине

    count - кол-во итемов в корзине
*/
function ShowItemCount(count){
    let basketspan = document.querySelector(basketitemscount);

    basketspan.style.display = 'inline-block';
    basketspan.innerText = count;
}

function RemoveItemCount(){
    let basketspan = document.querySelector(basketitemscount);

    basketspan.style.display = 'none';
}

function addToCartHandler(event){
    const itemId = event.currentTarget.getAttribute('data-item-id');

    ShowItemCount(itemsInCart.length + 1);
    addItemToCart(parseInt(itemId));
}

function removeToCartHandler(event){
    const itemId = event.currentTarget.getAttribute('data-item-id');
    let removeItem = document.querySelector('.basket__item[data-item-id="'+itemId+'"]');

    removeItem.innerHTML = '';
    let item = GetItemInBasketById(itemId);

    itemsInCart = itemsInCart.filter(
        function(e) {
          return e.id != itemId;
        },
    );

    AddBasketTotal();
    if(itemsInCart.length == 0)
        RemoveBasketTotal();

}

function addEventListenerToAddCart(){
    let addbtns = document.querySelectorAll('.add[data-item-id]');

    addbtns.forEach(button => {
        button.addEventListener('click', addToCartHandler);
    });
}

function addEventListenerToRemoveCart(){
    let removebtns = document.querySelectorAll('.basket__item-delete[data-item-id]');

    removebtns.forEach(button => {
        button.addEventListener('click', removeToCartHandler);
    });
}

insertItemsInFeatured(items);
addEventListenerToAddCart();