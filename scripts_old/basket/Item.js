

class Item {

    /*
        * @param {number} id - уникальный индефикатор товара
        * @param {string} name - имя товара
        * @param {string} description - описание товара
        * @param {image} image - картинка товара
        * @param {number} price - цена товара
        * @param {number} rating - рейтинг товара
    */

    constructor(id, name, description, price, image, rating){
        this.id = id;
        this.image = image;
        this.name = name;
        this.description = description;
        this.price = price;
        this.rating = rating;
    }

};